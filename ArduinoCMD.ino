#include <Arduino.h>
#include <FastLED.h>
#include <limits.h>
#include <stdint.h>

typedef uint8_t byte;

#define INT_PIN 2
#define LED_PIN 8
#define NUM_LEDS 8

CRGB led_data[NUM_LEDS];
char linebuf[128];

enum Commands
{
	LED,
	RGB_LED,
	HALT,
	RESET,
	CMDS
};

struct Command
{
	char cmd;
	void(*handler)(char *linebuf);
};

void readline(char *line)
{
	char c=0, i=0;
	while((c = readchar()) != '\r')
	{
		line[i++]=c;
	}
	line[i]='\r';
}

char readchar(void)
{
	char c=0;
	while(Serial.available() < 1); /* Wait until there's data to read */
	c = Serial.read();
	/*
	Serial.print(c);
	if(c == '\r')
		Serial.print("\n");
	*/
	return c;
}

/*void writechar(char c)
{
	if(c == '\n')
	{
		Serial.write("\r\n");
	}
	else
		Serial.write(c);
}*/

void ledtrigger(char *linebuf)
{
	int i = 0;
	int ret = 0;
	char a = 0;
	if((ret = digitalRead(LED_PIN)) == HIGH)
	{
		digitalWrite(LED_PIN, LOW);
	}
	else
	{
		digitalWrite(LED_PIN, HIGH);
	}
}

void rgb_led_init(void)
{
	int i = 0;
	for(i = 0; i < NUM_LEDS; i++)
		led_data[i] = CRGB(0,0,0);
	FastLED.show();
}

void rgb_led(char *linebuf)
{
	int i = 0;
	unsigned int pos = UINT_MAX;
	char r=0, g=0, b=0;

	sscanf(linebuf + 2, "%hu,%d,%d,%d\r", &pos, &r, &g, &b);
	if(pos != UINT_MAX)
		led_data[pos % NUM_LEDS] = CRGB(r, g, b);
	else
		rgb_led_init();
	FastLED.show();
}

void halt(char *linebuf)
{
	Serial.print("?HLT");
	while(1);
}

struct Command commands[] =
{
	[LED] =
	{
		.cmd='l',
		.handler=ledtrigger
	},
	[RGB_LED] =
	{
		.cmd='c',
		.handler=rgb_led
	},
	[HALT] =
	{
		.cmd='h',
		.handler=halt
	},
	[RESET] =
	{
		.cmd='r',
		.handler=0
	}
};

void getcmd(void)
{
	int i=0;
	Serial.print(">OK!");
	readline(linebuf);
	while(i < CMDS && linebuf[0] != '\r')
	{
		if(commands[i].cmd == linebuf[0])
		{
			commands[i].handler(linebuf);
			break;
		}
		else
			i++;
		if(i >= CMDS)
		{
			Serial.print("?CMD");
			delay(1000);
			break;
		}
	}
}

void int_handler(void)
{
}

void setup() {
	Serial.begin(57600);
	Serial.print(">PWR");

	pinMode(LED_PIN, OUTPUT);
	FastLED.addLeds<NEOPIXEL, 6>(led_data, NUM_LEDS);
	rgb_led_init();
	pinMode(INT_PIN, INPUT_PULLUP);
	attachInterrupt(digitalPinToInterrupt(INT_PIN), int_handler, CHANGE);
}

void loop() {
	getcmd();
}
